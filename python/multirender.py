#!/usr/bin/env python3

# Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
#
# This file is part of '1984 RayTracer'
#
# '1984 RayTracer' is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# '1984 RayTracer' is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import numpy as np
import os
import sys
import time as time_module

from render import Render
from ray import Ray

class Multirender(Render):
    """ Code for parallel render
        This code allows to launch several processes and render different lines in each one,
        all in parallel, allowing to take advantage of multicore systems
    """


    def __init__(self, scene, width, aspect = 16.0/9.0, angle = 45):
        """ Creates a renderer for the specified scene.
            @param scene The scene to render
            @param width The width of the output image in pixels
            @param aspect The aspect ratio for the output image
            @param angle The Field of Vision in degrees
        """
        super().__init__(scene, width, aspect, angle)
        self.set_processes(None)


    def set_processes(self, processes = None):
        """ Sets how many processes will be launched.
            @param processes Number of processes to use. If None, as many as cores will be used.
        """

        if processes is None:
            self._processes = 0
            with open("/proc/cpuinfo") as info:
                for line in info.readlines():
                    if line.startswith("processor"):
                        self._processes += 1
        else:
            self._processes = processes


    def render(self):
        """ Renders the current scene in parallel
            @param processes The number of parallel processes to use
        """

        self._start_time = time_module.time()

        # self._data will contain the pixels
        self._data = []

        self._childs = []

        # pout is used to send data from childs to parent
        pout = os.pipe()

        # launch the child processes
        for p in range(self._processes):
            # pin to send data from parent to child N
            pin = os.pipe()
            pid = os.fork()
            if pid == 0:
                self._render_child(p, pin[0], pout[1])
                sys.exit(0)
            self._childs.append({"id": p, "pid": pid, "pipe": pin[1], "working": False})

        y = 0 # first line
        while True:
            for child in self._childs:
                if not child["working"]:
                    # order that child to render the current line
                    # Command 0, two bytes with the line number to render
                    os.write(child["pipe"],bytearray([0]))
                    self._write_int(child["pipe"], y, 4)
                    child["working"] = True
                    self._print_current_line(y)
                    y += 1
                    if y == self._height:
                        break
            if y == self._height:
                break
            child_id = os.read(pout[0], 1)[0]
            self._childs[child_id]["working"] = False

        # Ended sending lines; now, wait for all children to end
        while True:
            working = False
            for child in self._childs:
                if child["working"]:
                    working = True
                    break
            if not working:
                break
            child_id = os.read(pout[0], 1)[0]
            self._childs[child_id]["working"] = False

        # Here we know that all childs ended. Ask each one their data
        lines = {}
        for child in self._childs:
            # command to send the data
            os.write(child["pipe"], bytearray([1]))
            while True:
                length = self._read_int(pout[0], 4)
                if length == 0: # there are no more lines
                    # read how many rays it used
                    os.write(child["pipe"], bytearray([3]))
                    Ray.counter += self._read_int(pout[0], 4)
                    # command to kill the child process
                    os.write(child["pipe"], bytearray([2]))
                    os.waitpid(child["pid"], 0)
                    break
                line_number = self._read_int(pout[0], 4)
                data = os.read(pout[0], length - 4)
                lines[line_number] = data
        # Recompose the image pixel by pixel, line by line
        for y in range(self._height):
            color = []
            counter = 0
            line = []
            for b in lines[y]:
                color.append(b)
                counter += 1
                if counter == 4:
                    line.append(color)
                    color = []
                    counter = 0
            self._data.append(line)
        print("")


    def _read_int(self, pipe, size):
        """ Reads SIZE bytes from a pipe and interprets them as an integer
            @param pipe The pipe from which read the bytes
            @param size The number of bytes that conforms the integer
            @return The integer
        """

        data = os.read(pipe, size)
        v = 0
        for i in data:
            v *= 256
            v += i
        return v


    def _write_int(self, pipe, value, size):
        """ Converts an integer into a bytearray, big endian format, and
            sends it over the specified pipe
            @param pipe The pipe into which write the bytes
            @param value The integer to convert
            @param size The number of bytes for the integer
            @return a bytearray with the integer in big endian format
        """

        v = []
        while value > 0:
            v.insert(0, value % 256)
            value = int(value / 256)
        while len(v) < size:
            v.insert(0, 0)
        os.write(pipe,bytearray(v))


    def _bytearray_to_int(self, barray):

        v = 0
        for i in barray:
            v *= 256
            v += i
        return v


    def _render_child(self, id, pin, pout):
        lines = {}
        while True:
            command = os.read(pin, 1)[0]
            if command == 0: # render line
                line_number = self._read_int(pin, 4)
                data = self._render_line(line_number)
                # store the line data
                lines[line_number] = data
                # notify the parent that we have ended and are waiting for the next line
                os.write(pout, bytearray([id]))
                continue
            if command == 1: # send lines
                for index in lines:
                    line = lines[index]
                    self._write_int(pout, 4 + 4 * len(line), 4) # number of bytes to send
                    self._write_int(pout, index, 4) # line number
                    data = []
                    for pixel in line:
                        for color in pixel:
                            data.append(color)
                    os.write(pout, bytearray(data))
                self._write_int(pout, 0, 4) # all lines sent
                continue
            if command == 2: # end
                sys.exit(0)
            if command == 3: # send number of rays
                self._write_int(pout, Ray.counter, 4)
                continue
