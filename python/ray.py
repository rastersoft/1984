#!/usr/bin/env python3

# Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
#
# This file is part of '1984 RayTracer'
#
# '1984 RayTracer' is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# '1984 RayTracer' is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import numpy as np

class Ray(object):

    def __init__(self, scene, origin, direction, osx, osy, time):
        """ Defines a ray, used to evaluate visibility with objects.
            @param scene The current scene
            @param origin The coordinates of the point where the ray originated
            @param direction A directional vector specifying the direction of the ray
            @param osx Oversampling coordinate in the X axis
            @param oxy Oversampling coordinate in the Y axis
            @param time The time instant that corresponds to this ray

            The oversampling coordinates allow to ensure a good ray distribution
            when having to evaluate lights.
        """

        self._scene = scene
        self._light_sources = 0

        self.time = time
        self.color = np.array([0,0,0,0])
        self.light_color = np.array([0,0,0,0], np.uint32)
        self.is_source = False
        self.normal = None
        self.specular = 0
        self.distance = None
        self.intersection_point = None
        self.object_id = None
        self.origin = origin
        self.direction = direction / np.linalg.norm(direction) # we need an unitary vector
        self.osx = osx
        self.osy = osy
        self.reflection_counter = 0
        Ray.counter += 1


    def evaluate_distance(self, object_id, point, color, normal, is_source, specular):
        """ Receives the intersection point, calculates the distance to the origin, and returns True if it is
            nearer than the last calculated distance

            @param object_id the identifier of the object being evaluated
            @param point A 3D np.array with the intersection point to evaluate
            @param color The new color to assign to the ray if this point is nearer than the previous
            @param is_source Whether the object is a light source or not
            @param specular A value between 0 and 1 that specifies how much specular has the object (0 no specular; 1 mirror)
            @return True if it is nearer than the last point; False if it is not """

        distance = np.linalg.norm(point - self.origin)
        if distance == 0:
            # the origin point must not be evaluated to allow to trace rays from one point of an object
            # to another point of the same object, like for refraction
            return
        if (self.distance is None) or (self.distance > distance):
            self.object_id = object_id
            self.distance = distance
            self.intersection_point = point
            self.color = np.array(color, np.int32)
            self.normal = normal
            self.is_source = is_source
            self.specular = specular


    def evaluate_ilumination(self, point, source_id):

        if self.object_id is None:
            return # there is no intersection with any of the objects in the scene

        if self.is_source:
            # a light source looks always iluminated, with the maximum intensity
            return

        # create a ray from the intersection point pointing to the light source's point
        # and evaluate if there is another object in-between

        direction = point - self.intersection_point
        direction = direction / np.linalg.norm(direction)
        # separate slightly the intersection point from the surface to avoid detecting it
        intersection_point = self.intersection_point + direction * 0.001
        ray = Ray(self._scene, intersection_point, direction, self.osx, self.osy, self.time)

        self._scene.evaluate_ray(ray)

        # if the nearest object is the same than source_id, then there are no objects
        # between this point and the light source, so it is iluminated

        if ray.object_id == source_id:
            # we calculate the cross product between the surface normal and the direction,
            # because it is proportional to the light intensity (Lambert's cosine theorem)

            #print("normal: {:}. direction {:}".format(self.normal, direction))
            intensity = np.dot(self.normal, direction)
            if intensity < self._scene.ambient:
                intensity = self._scene.ambient
            # since each component is between 0 and 255, we must divide between 255 after multiplying both
            # to scale again to 0-255 range.
            self.light_color = self.light_color + np.array(ray.color, np.uint32) * intensity
        else:
            # The object is in shadow
            intensity = self._scene.ambient
            self.light_color = self.light_color + np.array([255,255,255,255], np.uint32) * intensity
        self.color[3] = 255
        self._light_sources += 1


    def end_evaluate_ilumination(self):
        if self._light_sources != 0:
            self.color = np.array(self.color * self.light_color / (255 * self._light_sources), np.uint8)
            self.color[3] = 255


    def evaluate_specular(self):
        # if the object is absolutely not reflectant, don't waste resources evaluating it
        if self.specular <= 0:
            return

        if self.reflection_counter == self._scene.max_reflections:
            if self.specular >= 1:
                self.color = np.array([0,0,0,255], np.uint8)
            return

        # Calculate the reflected direction
        rdirection = self.direction - 2 * np.dot(self.direction, self.normal) * self.normal

        # Create a new ray
        ray = Ray(self._scene, self.intersection_point + rdirection * 0.0001, rdirection, self.osx, self.osy, self.time)
        ray.color = np.array([0,0,0,255], np.uint8)
        ray.reflection_counter = self.reflection_counter + 1
        # evaluate the reflected ray
        self._scene.evaluate(ray)
        self.color = np.array(self.color * (1 - self.specular) + ray.color * self.specular, np.uint8)
        self.color[3] = 255

Ray.counter = 0
