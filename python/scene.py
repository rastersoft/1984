#!/usr/bin/env python3

# Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
#
# This file is part of '1984 RayTracer'
#
# '1984 RayTracer' is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# '1984 RayTracer' is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import numpy as np
import random
import sys

class Scene(object):

    def __init__(self):
        self._elements = []
        self._light_sources = []
        self._id_counter = 0
        self.ambient = 0.15
        self._oversamplingX = 1
        self._oversamplingY = 1
        self._camera = None
        self.max_reflections = 3


    def set_oversampling(self, oversamplingX, oversamplingY):
        self._oversamplingX = oversamplingX
        self._oversamplingY = oversamplingY


    def append(self, obj):
        if obj.is_camera:
            raise Exception("Trying to add a camera as a 3D object")
        obj.identifier = self._id_counter
        self._id_counter += 1
        if self._camera is not None:
            obj.set_camera(self._camera)
        if obj.is_source:
            self._light_sources.append(obj)
        else:
            self._elements.append(obj)


    def set_camera(self, camera):
        if not camera.is_camera:
            raise Exception("Trying to add a 3D object as camera")

        if self._camera is None:
            self._camera = camera
            for obj in self._elements:
                obj.set_camera(camera)
            for obj in self._light_sources:
                obj.set_camera(camera)
        else:
            raise Exception("There is already a camera in the scene. Aborting")


    def evaluate(self, ray):
        """ Evaluates a ray and returns a Ray object with the result
            @param ray A Ray object with the ray to be evaluated
        """

        # test intersection with all the objects in the scene (except invisible lights)
        self.evaluate_ray(ray, False)

        # now evaluate the ilumination in the nearest object

        if ray.specular < 1:
            # evaluate the ilumination only if the object is not 100% reflectant
            for source in self._light_sources:
                # Ask the light source for a random point in its surface and the corresponding color
                point = source.get_light(ray, self._oversamplingX, self._oversamplingY)
                # And evaluate the color in that point
                ray.evaluate_ilumination(point, source.identifier)
            ray.end_evaluate_ilumination()

        # now evaluate the reflected ray
        ray.evaluate_specular()


    def evaluate_ray(self, ray, check_invisible = True):
        """ Evaluates a ray against all the objects
            @param ray The ray with the origin point and the direction
        """
        # test intersection with all the objects in the scene
        for obj in self._elements:
            obj.evaluate(ray)
        for obj in self._light_sources:
            if obj.visible or check_invisible:
                obj.evaluate(ray)
