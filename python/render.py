#!/usr/bin/env python3

# Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
#
# This file is part of '1984 RayTracer'
#
# '1984 RayTracer' is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# '1984 RayTracer' is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import numpy as np
import math
import random
import os
import time as time_module

from PIL import Image
from ray import Ray

class Render(object):

    def __init__(self, scene, width, aspect = 16.0/9.0, angle = 45):
        """ Creates a renderer for the specified scene.
            @param scene The scene to render
            @param width The width of the output image in pixels
            @param aspect The aspect ratio for the output image
            @param angle The Field of Vision in degrees
        """

        self.scene = scene
        self.fov = angle
        self._width = width
        self._height = int(width / aspect)
        if aspect < 1:
            self._distance = self._height / (2* math.tan(angle * math.pi / 360))
        else:
            self._distance = self._width / (2* math.tan(angle * math.pi / 360))
        self._data = None
        # By default, no oversampling, no jitter
        self._oversamplingX = 1
        self._oversamplingY = 1
        self._total_oversampling = 1


    def set_oversampling(self, x, y):
        """ Allows to set the oversampling for each pixel, in each of the two dimensions
            (it is common to set the same value in both parameters, but this gives more flexibility)

            @param x Oversampling in the X axis
            @param y Oversampling in the Y axis
        """
        self._oversamplingX = x
        self._oversamplingY = y
        self._total_oversampling = x * y
        self.scene.set_oversampling(x, y)


    def render(self):
        """ Renders the current scene with a single core"""

        self._start_time = time_module.time()

        # self._data will contain the pixels
        self._data = []

        # Do the tracing from -width/2 to width/2, and -height/2 to height/2;
        # this way the (0,0,0) coordinate is in the center of the screen
        for y in range(self._height):
            self._print_current_line(y)
            line = self._render_line(y)
            self._data.append(line)
        print("")


    def _print_current_line(self, y):
        now = time_module.time()
        if (now != self._start_time) and (y > 15):
            remaining = int((self._height - y) * (time_module.time() - self._start_time) / y)
            print("{:} of {:}. Running for {:} seconds. ERT: {:} seconds (ETA: {:}).            ".format(y , self._height, int(now - self._start_time), remaining, time_module.strftime("%a, %d %b %Y %H:%M:%S",time_module.localtime(now + remaining))), end="\r")
        else:
            print("{:} of {:}. Running for {:} seconds.".format(y , self._height, int(now - self._start_time)), end="\r")



    def _render_line(self, y):
        """ Renders a single scanline
            @param y The line number to render
            @return a list with color tuples, corresponding to the rendered line
        """

        w2 = int(self._width / 2)
        h2 = int(self._height / 2)
        line = []
        for x in range(self._width):
            # Now evaluate each ray that starts from the eye and passes through each pixel
            # By default, the eye is located at (0,0,0), and the distance between
            # the eye and the proyection plane is twice the height, to avoid
            # deformations in the objects located in the borders

            current_color = np.array([0,0,0,0])

            for overx in range(self._oversamplingX):
                for overy in range(self._oversamplingY):
                    # Each pixel is oversampled with oversamplingX * oversamplingY rays
                    # The pixel is divided in oversamplingX x oversamplingY subpixels and
                    # a ray is traced for each square. The precise point in the subpixel
                    # is choosen randomly, to make it a pseudo-poison disc distribution
                    ox = overx + random.random() * 0.75
                    oy = overy + random.random() * 0.75

                    ox /= self._oversamplingX
                    oy /= self._oversamplingY

                    # Calculate the time interval for this ray
                    # Divide the time in oversamplingX * oversamplingY intervals and
                    # distribute the rays into them, one for each interval.
                    # In each interval, the precise instant is random, like in the spatial
                    # oversampling, to have again a pseudo-poison disc distribution
                    time = (random.random() * 0.75 + overx + overy * self._oversamplingX) / self._total_oversampling

                    ray = Ray(self.scene, np.array([0,0,0,0]), np.array([x - w2 + ox, y - h2 + oy, self._distance,0], np.double), overx, overy, time)
                    self.scene.evaluate(ray)
                    current_color += ray.color

            # Calculate the average of all the rays sent for this pixel
            current_color = current_color / (self._oversamplingX * self._oversamplingY)
            line.append(np.array(current_color,np.uint8))
        return line


    def show(self):
        d = np.asarray(self._data, np.uint8)
        output = Image.fromarray(d)
        output.show()


    def save(self, filename):
        d = np.asarray(self._data, np.uint8)
        output = Image.fromarray(d)
        output.save(filename)
