#!/usr/bin/env python3

# Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
#
# This file is part of '1984 RayTracer'
#
# '1984 RayTracer' is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# '1984 RayTracer' is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


import numpy as np
import math
from PIL import Image
import sys

from sphere import Sphere
from plane import Plane
from camera import Camera
from render import Render
from multirender import Multirender
from scene import Scene
from ray import Ray

scene = Scene()
# add objects

specular_value = 0.15
displacement_range_ball8 = 1.5

class Ball9(Sphere):

    def __init__(self):
        super().__init__("../textures/ball_9.png")

    def set_current_time(self, time):
        if time == self._current_time:
            return
        self._current_time = time
        self.reset_matrix()
        self.specular = specular_value
        # move the number to the top (and a little more)
        self.rotate_x(math.pi / 8.0)
        self.rotate_y(-math.pi/2 + 0.3)
        # set ball size
        self.scale(5,5,5)
        if time < 0.5:
            self.rotate_x(time - 0.5)
            self.translate(-2,2-(0.5 - time) * 4,70)
        else:
            self.rotate_y(time - 0.5)
            self.translate(-2 - 4 * (time - 0.5),2,70)

class WhiteBall(Sphere):
    def __init__(self):
        super().__init__((255,255,224,255))

    def set_current_time(self, time):
        if time == self._current_time:
            return
        self._current_time = time
        self.reset_matrix()
        self.specular = specular_value
        self.scale(5,5,5)
        self.translate(time, -11, 70)

class Ball8(Sphere):
    def __init__(self):
        super().__init__("../textures/ball_8.png")

    def set_current_time(self, time):
        if time == self._current_time:
            return
        self._current_time = time
        self.reset_matrix()
        self.specular = specular_value
        self.rotate_y(-math.pi/2)
        angle = 0.2
        self.rotate_z(math.pi/4 - angle)
        self.scale(5,5,5)
        if time < 1/3:
            self.rotate_x(1/6)
            self.rotate_z(-math.pi/4)
            self.translate(5 , 9, 70)
        elif time < 2/3:
            displacement = time * displacement_range_ball8
            self.rotate_x((time * 3) / 6)
            self.rotate_z(-math.pi/4)
            self.translate(5 + displacement, 9 + displacement, 70)
        else:
            self.rotate_x(2/6)
            self.rotate_z(-math.pi/4)
            self.translate(5 + 0.66 * displacement_range_ball8, 9 + 0.66 * displacement_range_ball8, 70)

class Ball4(Sphere):
    def __init__(self):
        super().__init__("../textures/ball_4.png")

    def set_current_time(self, time):
        if time == self._current_time:
            return
        self._current_time = time
        self.reset_matrix()
        self.specular = specular_value
        self.rotate_y(-math.pi/2)
        self.rotate_x(-math.pi/4 + 0.3)
        self.rotate_z(math.pi/4)
        self.scale(5,5,5)
        if time < 2/3:
            self.rotate_z(-math.pi/4)
            self.translate(12.1 + 0.66 * displacement_range_ball8, 16.1 + 0.66 * displacement_range_ball8, 70)
        else:
            self.rotate_x((time - 2/3) * 1.5)
            self.rotate_z(-math.pi/4)
            self.translate(12.1 + 0.66 * displacement_range_ball8 + (time - 0.66666) / 2, 16.1 + 0.66 * displacement_range_ball8 + (time - 0.66666) / 2, 70)

sp1 = Sphere("../textures/ball_1.png")
sp1.specular = specular_value
sp1.scale(5,5,5)
sp1.rotate_y(-math.pi/2 - 0.4)
sp1.rotate_x(0.1)
sp1.translate(-17,5,70)

sp9 = Ball9()
spw = WhiteBall()
sp8 = Ball8()
sp4 = Ball4()

pl1 = Plane("../textures/window.png")
pl1.scale(200,109,1)
pl1.rotate_y(math.pi/2)
pl1.rotate_x(-math.pi/2)
pl1.translate(130,30,30)

pl2 = Plane("../textures/neon.png")
pl2.scale(250,109,1)
pl2.rotate_y(math.pi/2)
pl2.rotate_x(math.pi/2)
pl2.translate(-130,30,30)

# billiard table
pl5 = Plane("../textures/table.png")
pl5.scale(40,40,1)
pl5.translate(0,0,75)


# sources
light1 = Sphere((255,255,255,255), True)
light1.scale(15,15,15)
light1.visible = True
light1.translate(-30,-10,-45)

light2 = Sphere((255,255,255,255), True)
light2.scale(15,15,15)
light2.visible = True
light2.translate(-30,120,-45)

light3 = Sphere((255,255,255,255), True)
light3.scale(15,15,15)
light3.visible = True
light3.translate(-30,-140,-45)

camera = Camera()
camera.rotate_z(-0.4)
camera.translate(-3,-1,7)
#camera.translate(0,0,250)
scene.set_camera(camera)

scene.append(sp1)
scene.append(sp9)
scene.append(spw)
scene.append(sp8)
scene.append(sp4)
scene.append(pl1)
scene.append(pl2)
scene.append(pl5)
scene.append(light1)
scene.append(light2)
scene.append(light3)

if True:
    r = Multirender(scene, 300)
else:
    r = Render(scene, 320)
#r.set_oversampling(8,8)
r.render()

print("\nEmited {:} rays".format(Ray.counter))
if len(sys.argv) > 1:
    r.save(sys.argv[1])
r.show()
