#!/usr/bin/env python3

# Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
#
# This file is part of '1984 RayTracer'
#
# '1984 RayTracer' is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# '1984 RayTracer' is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import numpy as np
import random

from object3d import Object3d

class Plane(Object3d):
    """ This object is an plane, which currently can only have a single color. You must
        create a new object for each object that you want to add to an scene

        All planes, by default, are located at the plane XY, and they extends from (-1, -1)
        to (1,1). It is possible to change this by using the ROTATE, TRANSLATE and SCALE
        methods. """


    def __init__(self, texture, is_source = False):
        """ Initializates the plane
            @param texture The texture or color for the surface
            @param is_source A boolean specifying if this object is a light source
        """
        super().__init__(texture, is_source)
        # precalculate these values to reduce the time needed
        self._normal = np.array([0,0,1,0],np.double)


    def get_light(self, ray, oversamplingX, oversamplingY):
        """ Return a point from the surface
            @return an np.array with a point from the surface of the plane
        """

        super().get_light(ray, oversamplingX, oversamplingY)

        lx = (2 * (ray.osx + random.random()) / oversamplingX) - 1
        ly = (2 * (ray.osy + random.random()) / oversamplingY) - 1

        point = np.array([lx,ly,0], np.double)
        return self._object_to_scene_coords(point)


    def evaluate(self, ray):
        """ Evaluates a ray, checking if the intersection with this plane is the nearest one and
            setting the current color

            @param ray A Ray object to evaluate its intersection
        """

        super().evaluate(ray)

        # First, take the ray values (which are in scene coordinates) and transform them into
        # object coordinates.
        o, l = self._ray_to_object_coords(ray)

        # Now calculate the intersection between the transformed ray and the XY plane
        tmp = np.dot(l, self._normal)
        # Ensure that the normal always face to the viewer, to guarantee that the ilumination works as expected
        if tmp < 0:
            normal = self._normal
        else:
            normal = -self._normal
        if tmp == 0:
            # No intersection, the ray and the plane are parallel
            return
        d = np.dot(- o, self._normal) / tmp
        if d < 0:
            # reject rays that intersect behind the origin point
            return
        intersection = o + l * d
        if (intersection[0] > 1) or (intersection[0] < -1) or (intersection[1] > 1) or (intersection[1] < -1):
            return
        point = self._object_to_scene_coords(intersection) # get the true intersection coordinates in scene space
        normal = self._transform_normal_coordinates(normal)
        ray.evaluate_distance(self.identifier,
                              point,
                              self._get_color_at( ( (intersection[0] + 1) / 2, (intersection[1] + 1) / 2 ) ),
                              normal,
                              self.is_source,
                              self.specular)
