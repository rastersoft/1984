#!/usr/bin/env python3

# Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
#
# This file is part of '1984 RayTracer'
#
# '1984 RayTracer' is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# '1984 RayTracer' is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import numpy as np
import math
import random

from object3d import Object3d

class Sphere(Object3d):
    """ This object is an sphere, which currently can only have a single color. You must
        create a new object for each object that you want to add to an scene.

        All spheres, by default, are located at the origin and have a radius of 1.
        It is possible to change this by using the ROTATE, TRANSLATE and SCALE
        methods. """


    def __init__(self, texture, is_source = False):
        """ Initializates the sphere
            @param texture The texture or color for the surface
            @param is_source A boolean specifying if this object is a light source
        """
        super().__init__(texture, is_source)


    def _point_to_texture(self, point):
        """ Receives an intersection point and returns the angular coordinates, for the texture
            @param point The intersection point. It must be in the surface of the sphere
            @return a 2-element tupla with the texture coordinates in the range [0,1]
        """

        x = point[0]
        y = point[1]
        z = point[2]
        # just in case. Values should never be outside [-1,1], but rounding errors can produce them
        y = max(-1, y)
        y = min(y, 1)
        v = math.asin(y) # the arcsin of Y is the angle
        tmp = math.cos(v) # the radius of the circle in that point
        v /= math.pi
        v += 0.5
        if tmp == 0:
            return 0, v
        # normalize X and Z
        x /= tmp
        z /= tmp
        x = max(-1, x)
        x = min(x, 1)
        z = max(-1, z)
        z = min(z, 1)
        u = math.acos(x)
        if z <= 0:
            u = (2 * math.pi) - u
        u /= (2 * math.pi)
        return (u, v)


    def get_light(self, ray, oversamplingX, oversamplingY):
        """ Returns a point from the surface, to calculate ilumination
            @param ray The ray which is being evaluated for ilumination
            @param oversamplingX The oversampling value in the X axis
            @param oversamplingY The oversampling value in the Y axis
            @return an np.array with a point from the surface of the sphere
        """

        super().get_light(ray, oversamplingX, oversamplingY)

        a = random.random() * math.pi * ray.osx / oversamplingX
        b = random.random() * math.pi * 2 * ray.osy / oversamplingY

        y = math.sin(a)
        radius = math.cos(a)
        z = radius * math.sin(b)
        x = radius * math.cos(b)

        point = np.array([x,y,z,0], np.double)
        return self._object_to_scene_coords(point)


    def evaluate(self, ray):
        """ Evaluates a ray, checking if the intersection with this sphere is the nearest one and
            setting the current color

            @param ray A Ray object to evaluate its intersection
        """

        super().evaluate(ray)

        # First, take the ray values (which are in scene coordinates) and transform them into
        # object coordinates.
        o, l = self._ray_to_object_coords(ray)

        # Now calculate the intersection between the transformed ray and an sphere of radius 1 located
        # at (0,0,0)
        tmp = math.pow(np.dot(l, o), 2) - (math.pow(np.linalg.norm(o),2) - 1)
        if tmp < 0:
            # No intersection
            return
        tmp = math.sqrt(tmp)
        tmp2 = -np.dot(l, o)
        if (tmp2 - tmp) >= 0:
            # Only if tmp2-tmp is positive, the ray goes "forward"; reject if it goes "backwards" because that
            # means that the intersection is behind the origin point
            intersection1 = o + l * (tmp2 - tmp)
            normal1 = self._transform_normal_coordinates(intersection1)
            point1 = self._object_to_scene_coords(intersection1) # get the true intersection coordinates in scene space
            ray.evaluate_distance(self.identifier,
                                  point1,
                                  self._get_color_at(self._point_to_texture(intersection1)),
                                  normal1,
                                  self.is_source,
                                  self.specular)
        if (tmp != 0) and ((tmp2 + tmp) >= 0):
            # if tmp is 0, the ray is tangent (so there is a single intersection point);
            # if it is greater than 0, it intersects in two points
            intersection2 = o + l * (tmp2 + tmp)
            normal2 = self._transform_normal_coordinates(intersection2)
            point2 = self._object_to_scene_coords(intersection2) # get the true intersection coordinates in scene space
            ray.evaluate_distance(self.identifier,
                                  point2,
                                  self._get_color_at(self._point_to_texture(intersection2)),
                                  normal2,
                                  self.is_source,
                                  self.specular)
