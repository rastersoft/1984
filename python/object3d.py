#!/usr/bin/env python3

# Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
#
# This file is part of '1984 RayTracer'
#
# '1984 RayTracer' is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# '1984 RayTracer' is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import numpy as np
from PIL import Image

class Object3d(object):
    """ Base class for every object in the scene.
        It contains the current transform matrix and its inverse. This allows
        to easily convert object points into space points and vice-versa.
        It also offers several functions to modify both matrixes, like rotate,
        translate and scale.
    """

    def __init__(self, texture, is_source = False):
        """ Constructor

            @param texture It specifies the texture/colors to use. It can be:
                * a tuple of four integers between 0 and 255, in which case the element will have an uniform color
                * an array of several color tuples, in which case the element will have a checkered surface with those colors.
                  example: [(0,0,0,255),(255,255,255,255)] will create a 2x2 black and white checkered pattern;
                  example: [(255,0,0,255),(0,255,0,255)] * 4 will create a 8x8 red and green checkered pattern;
                * an string with the path to a picture, which will be used as texture
        """

        self.identifier = None
        self.visible = True
        self.specular = 0
        self.is_source = is_source
        self.is_camera = False

        self._stack = []
        # by default set an impossible instant, to ensure that the first time the
        # position is always set
        self._current_time = -1
        self._camera = None
        self._applied_camera = False

        self.reset_matrix()

        if texture is None:
            self.color = (0,0,0,255)
            self._texture = None
        elif isinstance(texture, str):
            # passing an string means we want to use a picture as texture
            picture = Image.open(texture).convert("RGBA")
            self._texture = list(picture.getdata())
            self._texture_width, self._texture_height = picture.size
        elif isinstance(texture, list) and (len(texture) > 0) and isinstance(texture[0], tuple):
            # a list of lists or tuples means we want a checkered picture with the specified colors
            self._texture = []
            self._texture_width = len(texture)
            self._texture_height = len(texture)
            for a in range(len(texture)):
                self._texture += texture[a:] + texture[:a]
        elif isinstance(texture, tuple) and (len(texture) == 4) and isinstance(texture[0], int):
            # only a tuple of integers means a single color
            self.color = texture
            self._texture = None
        else:
            raise Exception("Error: {:} is not a valid texture".format(texture))


    def set_camera(self, camera):
        if self._camera is None:
            self._camera = camera


    def _apply_camera(self):
        if (self._camera is None) or (self._applied_camera):
            return
        self._current_matrix = np.dot(self._current_matrix, self._camera.get_matrix())
        # since the camera is the last thing to aply, we can safely wait until now to calculate the inverse
        self._current_inverse = np.linalg.inv(self._current_matrix)
        # precalculate the origin to speed up normal and other directions calculation
        self._origin = np.dot(np.array([0,0,0,1], np.double), self._current_matrix)
        # to ensure that norm and other operations over a vector gives the same result for a 3D and a 4D vector,
        # the fourth coordinate must be zero
        self._origin[3] = 0
        self._applied_camera = True


    def reset_matrix(self):
        """ Sets the current and inverse matrixes as the identity, to start again. Useful when using motion blur """

        self._current_matrix = np.identity(4,np.double)
        self._current_inverse = np.identity(4,np.double)
        self._origin = np.array([0,0,0,0],np.double)
        self._applied_camera = False


    def set_current_time(self, time):
        """ Specifies the new time instant, to allow to evaluate motion blur

            @param time The time value, in [0, 1] range, being 0 the instant when the shutter opens, and 1 when it closes
        """

        # by default, objects are fully static. Moving objects must inherite from the class and overwrite this method,
        # setting the current matrix with the right value for the time instant

        # This method is called every time a ray is evaluated (to avoid having to relocate objects that aren't being
        # evaluated), so it is paramount to store the instant in self._current_time, and don't recalculate the matrixes
        # unless the value is different.

        self._current_time = time
        return


    def _get_color_at(self, coordinates):
        """ Returns the pixel value for the coordinates U,V, being both float values in range [0, 1]
            It uses the pixels from the texture if available, of the color if there is no texture.

            @param coordinates a 2D tuple with the U and V coordinates in the texture
            @return a 4-element tupla with the RGBA value
        """

        if self._texture is None:
            return self.color

        height = min(int(coordinates[1] * self._texture_height), self._texture_height - 1)
        width = min(int(coordinates[0] * self._texture_width), self._texture_width - 1)
        return self._texture[width + height * self._texture_width]


    def _transform_normal_coordinates(self, normal):
        """ Returns the normal in the scene coordinates
            @param normal The normal in object coordinates
            @return The normal in scene coordinates
        """

        # Add the origin to transform it into a point in 3D space
        # (but in object coordinates it is (0,0,0), so that's why there isn't normal + origin)
        # transform it to scene coordinates, and substract the transformed origin to convert
        # it again in a direction vector,

        normal = self._object_to_scene_coords(normal) - self._origin
        # it must be an unitary vector
        normal = normal / np.linalg.norm(normal)
        return normal


    def _scene_to_object_coords(self, coordinates):
        """ Takes a vector of coordinates in the scene coordinate system, and returns them
            transformed in the object coordinates, by applying the inverse of the transform
            matrix

            @param coordinates A vector of four coordinates, as an np.array
            @return a vector in the object coordinates
        """
        # The coordinates are always cuaternions, to allow to be multiplied directly with the transformation matrixes
        # But to make the NORM and other operations give the same result with a 4D vector than with a 3D vector, not only
        # the first, second and third coordinates must be the same, but the fourth one in the 4D vector must be zero.
        # But to operate with the transformation matrixes, the fourth element must be one,
        # so first we set the fourth element to 1
        coordinates[3] = 1
        # Transform the coordinates (that, now, are a proper cuaternion)
        coordinates2 = np.dot(coordinates, self._current_inverse)
        # and restore the fourth element to 0, in the result AND in the parameter, to ensure that both can be used
        # as coordinates
        coordinates[3] = 0
        coordinates2[3] = 0
        return coordinates2


    def _object_to_scene_coords(self, coordinates):
        """ Takes a vector of coordinates in the object coordinate system, and returns them
            transformed in the scene coordinates, by applying the current transform matrix

            @param coordinates A vector of four coordinates, as an np.array
            @return a vector in the scene coordinates
        """
        # Transform the coordinates into a cuaternion, to be able to operate with it
        coordinates[3] = 1
        # Transform the coordinates
        coordinates2 = np.dot(coordinates, self._current_matrix)
        coordinates[3] = 0
        coordinates2[3] = 0
        return coordinates2


    def _ray_to_object_coords(self, ray):
        """ Transforms the origin and direction vectors of a ray (which are in scene coordinates)
            into object coordinates.

            @param ray A ray
            @return a tupla with the origin and the direction transformed according to the current matrix
        """
        o = self._scene_to_object_coords(ray.origin)
        l = self._scene_to_object_coords(ray.direction + ray.origin) - o
        l = l / np.linalg.norm(l) # put it again as an unitary vector
        return o, l


    def rotate_x(self, angle):
        """ Applies a rotation around the X axis.
            @param angle The rotation angle, in radians
        """
        sin = np.sin(angle)
        cos = np.cos(angle)
        m1 = np.array([[1,0,0,0],[0,cos,-sin,0],[0,sin,cos,0],[0,0,0,1]],np.double)
        self._current_matrix = np.dot(self._current_matrix, m1)


    def rotate_y(self, angle):
        """ Applies a rotation around the Y axis.
            @param angle The rotation angle, in radians
        """
        sin = np.sin(angle)
        cos = np.cos(angle)
        m1 = np.array([[cos,0,-sin,0],[0,1,0,0],[sin,0,cos,0],[0,0,0,1]],np.double)
        self._current_matrix = np.dot(self._current_matrix, m1)


    def rotate_z(self, angle):
        """ Applies a rotation around the Z axis.
            @param angle The rotation angle, in radians
        """
        sin = np.sin(angle)
        cos = np.cos(angle)
        m1 = np.array([[cos,-sin,0,0],[sin,cos,0,0],[0,0,1,0],[0,0,0,1]],np.double)
        self._current_matrix = np.dot(self._current_matrix, m1)


    def translate(self, x, y, z):
        """ Applies a translation along the X, Y and Z axis
            @param x How many units to move the object along the X axis
            @param y How many units to move the object along the Y axis
            @param z How many units to move the object along the Z axis
        """
        m1 = np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[x,-y,z,1]],np.double)
        self._current_matrix = np.dot(self._current_matrix, m1)


    def scale(self, x, y, z):
        """ Applies an scale transformation along the X, Y and Z axis
            @param x The scale value along the X axis
            @param y The scale value along the Y axis
            @param z The scale value along the Z axis
        """
        m1 = np.array([[x, 0, 0, 0],[0,y,0,0],[0,0,z,0],[0,0,0,1]],np.double)
        self._current_matrix = np.dot(self._current_matrix, m1)


    def get_light(self, ray, oversamplingX, oversamplingY):
        """ This call must be overriden by each new object.

            Returns a point from the surface, to calculate ilumination
            @param ray The ray which is being evaluated for ilumination
            @param oversamplingX The oversampling value in the X axis
            @param oversamplingY The oversampling value in the Y axis
            @return an np.array with a point from the surface of the object
        """

        # before everything, update the position acording to the time instant of the ray being evaluated
        self.set_current_time(ray.time)

        # apply the camera transformation if needed
        self._apply_camera()

        return np.array([0,0,0,0])


    def evaluate(self, ray):
        """ This call must be overriden by each new object. It must evaluate
            the intersection of the object with the ray to set the color
            inside it """

        # before everything, update the position acording to the time instant of the ray being evaluated
        self.set_current_time(ray.time)

        # apply the camera transformation if needed
        self._apply_camera()
