// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>


#include <cstdio>
#include <stdint.h>

#include "sphere.hh"
#include "plane.hh"
#include "camera.hh"
#include "render.hh"
#include "multirender.hh"
#include "scene.hh"
#include "color.hh"
#include "defines.hh"

float specular_value = 0.15;
float displacement_range_ball8 = 1.5;

class MovingBall: public Sphere {
protected:
    virtual void set_position(double time_start, double time_end, double rotation_angle, double rotation_value_start, double rotation_value_end, class Vector &position_start, class Vector &position_end ) {
        if ((this->current_time >= time_start) && (this->current_time < time_end)) {
            double interval = (this->current_time - time_start) / (time_end - time_start);
            //printf("%f -> %f - %f: %f\n",this->current_time, time_start, time_end, interval);
            this->rotate_z(rotation_angle);
            this->rotate_x(rotation_value_start + (interval * (rotation_value_end - rotation_value_start)));
            this->rotate_z(-rotation_angle);
            Vector position = position_start + (position_end - position_start) * interval;
            this->translate(position.values[0], position.values[1], position.values[2]);
        }
    }
public:
    MovingBall(Texture *texture) : Sphere(texture) {};
};

class Ball9: public Sphere {

public:
    Ball9() : Sphere(new Texture("../textures/ball_9.png")) {
        this->specular = specular_value;
    }

    virtual void set_current_time(double time) {
        if (time == this->current_time) {
            return;
        }
        this->current_time = time;
        this->reset_matrix();
        // move the number to the top (and a little more)
        this->rotate_x(PI/8.0);
        this->rotate_y(-PI/2.0 + 0.3);
        // set ball size
        this->scale(5,5,5);
        if (time < 0.5) {
            this->rotate_x(time - 0.5);
            this->translate(-2,2-(0.5 - time) * 4,70);
        } else {
            this->rotate_y(time - 0.5);
            this->translate(-2 - 4 * (time - 0.5),2,70);
        }
    }
};


class BallWhite: public Sphere {

public:
    BallWhite() : Sphere(Color(1.0,1.0,0.8784,1.0)) {
        this->specular = specular_value;
    }

    virtual void set_current_time(double time) {
        if (time == this->current_time) {
            return;
        }
        this->current_time = time;
        this->reset_matrix();
        this->scale(5,5,5);
        this->translate(time, -11, 70);
    }
};


class Ball8: public MovingBall {

private:
    Vector initial = Vector(5, 9, 70);
    Vector final = Vector(5 + displacement_range_ball8, 9 + displacement_range_ball8, 70);

public:
    Ball8() : MovingBall(new Texture("../textures/ball_8.png")) {
        this->specular = specular_value;
    }

    virtual void set_current_time(double time) {
        if (time == this->current_time) {
            return;
        }
        this->current_time = time;
        this->reset_matrix();
        this->rotate_y(-PI/2.0);
        this->scale(5,5,5);
        float angle = PI/4.0 - 0.2;
        this->set_position(0, 1.0 / 3.0, angle, 0, 0, this->initial, this->initial);
        this->set_position(1.0/3.0, 2.0/3.0, angle, 0, 1.0/3.0, this->initial, this->final);
        this->set_position(2.0/3.0, 1, angle, 1.0/3.0, 1.0/3.0, this->final, this->final);
    }
};


class Ball4: public MovingBall {

private:
    Vector initial = Vector(12.07 + displacement_range_ball8, 16.07 + displacement_range_ball8, 70);
    Vector final = Vector(12.07 + displacement_range_ball8 * 1.5, 16.07 + displacement_range_ball8 * 1.5, 70);

public:
    Ball4() : MovingBall(new Texture("../textures/ball_4.png")) {
        this->specular = specular_value;
    }

    virtual void set_current_time(double time) {
        if (time == this->current_time) {
            return;
        }
        this->current_time = time;
        this->reset_matrix();
        this->rotate_y(-PI/2.0);
        this->rotate_x(-PI/4.0 + 0.3);
        this->scale(5,5,5);
        double angle = PI/4.0;
        this->set_position(0, 2.0 / 3.0, angle , 0, 0, this->initial, this->initial);
        this->set_position(2.0 / 3.0, 1, angle, 0, 0.45, this->initial, this->final);
    }
};


int main(int argc, char**argv) {

    class Scene scene;
    // add objects

    Sphere *sp1 = new Sphere(new Texture("../textures/ball_1.png"));
    sp1->specular = specular_value;
    sp1->scale(5,5,5);
    sp1->rotate_y(-PI/2.0 - 0.4);
    sp1->rotate_x(0.1);
    sp1->translate(-17,5,70);

    Sphere *sp9 = new Ball9();
    Sphere *spw = new BallWhite();
    Sphere *sp8 = new Ball8();
    Sphere *sp4 = new Ball4();

    Plane *pl1 = new Plane(new Texture("../textures/window.png"));
    pl1->scale(200,109,1);
    pl1->rotate_y(PI/2.0);
    pl1->rotate_x(-PI/2.0);
    pl1->translate(130,30,30);

    Plane *pl2 = new Plane(new Texture("../textures/neon.png"));
    pl2->scale(250,109,1);
    pl2->rotate_y(-PI/2.0);
    pl2->rotate_x(-PI/2.0);
    pl2->translate(-130,30,30);

    // billiard table
    Plane *pl5 = new Plane(new Texture("../textures/table.png"));
    pl5->scale(45,45,1);
    pl5->translate(0,0,75);

    // sources
    Sphere *light1 = new Sphere(Color(1,1,1,1), true);
    light1->scale(15,15,15);
    light1->visible = true;
    light1->translate(-30,-10,-45);

    Sphere *light2 = new Sphere(Color(1,1,1,1), true);
    light2->scale(15,15,15);
    light2->visible = true;
    light2->translate(-30,120,-45);

    Sphere *light3 = new Sphere(Color(1,1,1,1), true);
    light3->scale(15,15,15);
    light3->visible = true;
    light3->translate(-30,-140,-45);

    Camera *camera = new Camera();
    camera->rotate_z(-0.4);
    camera->translate(-3,-1,7);
    //camera->translate(0,0,850); // allows to see the full scene from far away
    scene.set_camera(camera);

    scene.append(sp1);
    scene.append(sp9);
    scene.append(spw);
    scene.append(sp8);
    scene.append(sp4);
    scene.append(pl1);
    scene.append(pl2);
    scene.append(pl5);
    scene.append(light1);
    scene.append(light2);
    scene.append(light3);

#if 1
    Multirender r(&scene, 2560);
#else
    Render r(&scene, 600);
#endif
    r.set_oversampling(8, 8);
    r.render();

    if (argc > 1) {
        r.save(argv[1]);
    } else {
        r.show();
    }
    return 0;
}
