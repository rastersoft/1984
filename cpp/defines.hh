#ifndef _DEFINES_H_
#define _DEFINES_H_

#include <stdlib.h>

#define RANDOM (((double) random()) / ((double)RAND_MAX))
#define PI 3.141592

#define MAX(a, b) (((a)>(b)) ? (a) : (b))
#define MIN(a, b) (((a)<(b)) ? (a) : (b))

#endif // _DEFINES_H_
