// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#include <cstdio>

#include "matrix.hh"

Matrix::Matrix() {
    this->reset();
}

void Matrix::reset() {
    // Sets the matrix as the identity matrix
    int i;
    double *p;
    for(i=0, p = this->values; i<16; i++) {
        if (i%5 == 0) {
            *p = 1.0;
        } else {
            *p = 0.0;
        }
        p++;
    }
}

void Matrix::rdot(Matrix *matrix) {
    this->dot(this, matrix);
}

void Matrix::ldot(Matrix *matrix) {
    this->dot(matrix, this);
}

void Matrix::dot(Matrix *left, Matrix *right) {

    double acc,*p1,*p2,*pd,output[16];
    pd = output;
    p1 = left->values;
    p2 = right->values;

    for(int y=0; y<4;y++) {
        for(int x=0; x<4; x++) {
            acc = 0;
            for(int z=0; z<4; z++) {
                acc += (*p1) * (*p2);
                p1++;
                p2+=4;
            }
            *pd = acc;
            pd++;
            p1 -= 4;
            p2 -= 15;
        }
        p1 += 4;
        p2 -= 4;
    }
    int c;
    for(p1=this->values,p2=output,c=0;c<16;c++) {
        *(p1++) = *(p2++);
    }
}

void Matrix::print() {
    double *p = this->values;
    printf("(");
    for(int y = 0; y<4; y++) {
        printf("(");
        for (int x = 0;x<4; x++) {
            printf("%f, ",*p);
            p++;
        }
        printf(")\n");
    }
    printf(")\n");
}
