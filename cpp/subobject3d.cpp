// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#include <cstdio>

#include "subobject3d.hh"


void SubObject3d::rotate_x(double angle) {
    /** Applies a rotation around the X axis.
        @param angle The rotation angle, in radians
    */
    Matrix m1;
    double sinv = sin(angle);
    double cosv = cos(angle);
    m1.values[5] = cosv;
    m1.values[6] = -sinv;
    m1.values[9] = sinv;
    m1.values[10] = cosv;
    this->current_matrix.rdot(&m1);
    m1.values[6] = sinv;
    m1.values[9] = -sinv;
    this->current_inverse.ldot(&m1);
}

void SubObject3d::rotate_y(double angle) {
    /** Applies a rotation around the Y axis.
        @param angle The rotation angle, in radians
    */
    Matrix m1;
    double sinv = sin(angle);
    double cosv = cos(angle);
    m1.values[0] = cosv;
    m1.values[2] = -sinv;
    m1.values[8] = sinv;
    m1.values[10] = cosv;
    this->current_matrix.rdot(&m1);
    m1.values[2] = sinv;
    m1.values[8] = -sinv;
    this->current_inverse.ldot(&m1);
}

void SubObject3d::rotate_z(double angle) {
    /** Applies a rotation around the Z axis.
        @param angle The rotation angle, in radians
    */
    Matrix m1;
    double sinv = sin(angle);
    double cosv = cos(angle);
    m1.values[0] = cosv;
    m1.values[1] = -sinv;
    m1.values[4] = sinv;
    m1.values[5] = cosv;
    this->current_matrix.rdot(&m1);
    m1.values[1] = sinv;
    m1.values[4] = -sinv;
    this->current_inverse.ldot(&m1);
}

void SubObject3d::translate(double x, double y, double z) {
    /** Applies a translation along the X, Y and Z axis
        @param x How many units to move the object along the X axis
        @param y How many units to move the object along the Y axis
        @param z How many units to move the object along the Z axis
    */
    Matrix m1;
    m1.values[12] = x;
    m1.values[13] = -y;
    m1.values[14] = z;
    this->current_matrix.rdot(&m1);
    m1.values[12] = -x;
    m1.values[13] = y;
    m1.values[14] = -z;
    this->current_inverse.ldot(&m1);
}

void SubObject3d::scale(double x, double y, double z) {
    /** Applies an scale transformation along the X, Y and Z axis
        @param x The scale value along the X axis
        @param y The scale value along the Y axis
        @param z The scale value along the Z axis
    */
    if ((x == 0) || (y == 0) || (z == 0)) {
        printf("Error defining scale\n");
        exit(-1);
    }
    Matrix m1;
    m1.values[0] = x;
    m1.values[5] = y;
    m1.values[10] = z;
    this->current_matrix.rdot(&m1);
    m1.values[0] = 1/x;
    m1.values[5] = 1/y;
    m1.values[10] = 1/z;
    this->current_inverse.ldot(&m1);
}
