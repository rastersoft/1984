// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#include <cstdio>

#include "plane.hh"
#include "defines.hh"

/*
    This object is an plane, which currently can only have a single color. You must
    create a new object for each object that you want to add to an scene

    All planes, by default, are located at the plane XY, and they extends from (-1, -1)
    to (1,1). It is possible to change this by using the ROTATE, TRANSLATE and SCALE
    methods. */


Plane::Plane(class Texture* texture, bool is_source):Object3d(texture, is_source) {
        /*Initializates the plane
            @param texture The texture or color for the surface
            @param is_source A boolean specifying if this object is a light source
        */
        // precalculate these values to reduce the time needed
        this->normal.values[2] = 1.0;
}

Plane::Plane(class Color color, bool is_source):Object3d(color, is_source) {
        /*Initializates the plane
            @param texture The texture or color for the surface
            @param is_source A boolean specifying if this object is a light source
        */
        // precalculate these values to reduce the time needed
        this->normal.values[2] = 1.0;
}

void Plane::get_light(class Ray &ray, uint8_t oversamplingX, uint8_t oversamplingY, Vector & point) {
    /*Return a point from the surface
        @return an np.array with a point from the surface of the plane
    */

    Object3d::get_light(ray, oversamplingX, oversamplingY, point);

    point.values[0] = (2.0 * (ray.osx + RANDOM) / oversamplingX) - 1.0;
    point.values[1] = (2.0 * (ray.osy + RANDOM) / oversamplingY) - 1.0;

    point *= this->current_matrix;
}

void Plane::evaluate(class Ray &ray) {
    /*
        Evaluates a ray, checking if the intersection with this plane is the nearest one and
        setting the current color

        @param ray A Ray object to evaluate its intersection
    */

    Vector o;
    Vector l;

    Object3d::evaluate(ray);

    // First, take the ray values (which are in scene coordinates) and transform them into
    // object coordinates.
    this->ray_to_object_coords(ray, o, l);

    // Now calculate the intersection between the transformed ray and the XY plane
    double tmp = Vector::dot(l, this->normal);
    // Ensure that the normal always face to the viewer, to guarantee that the ilumination works as expected

    if (tmp == 0) {
        // No intersection, the ray and the plane are parallel
        return;
    }

    Vector normal(this->normal);
    if (tmp > 0) {
        normal *= -1;
    }

    double d = Vector::dot(o * -1, this->normal) / tmp;
    if (d < 0) {
        // reject rays that intersect behind the origin point
        return;
    }
    Vector intersection = o + l * d;

    if ((abs(intersection.values[0]) > 1) || (abs(intersection.values[1]) > 1)) {
        return;
    }
    Vector point(intersection);
    point *= this->current_matrix;
    this->transform_normal_coordinates(normal);
    Color color;
    this->get_color_at((intersection.values[0] + 1.0) / 2.0, (intersection.values[1] + 1.0) / 2.0, color);
    ray.evaluate_distance(this->identifier, point, color, normal, this->is_source, this->specular);
}
