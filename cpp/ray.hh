// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef _RAY_H_
#define _RAY_H_

#include <stdint.h>

#include "scene.hh"
#include "vector.hh"
#include "color.hh"

class Ray {

private:
    uint32_t light_sources;

public:
    class Scene *scene;
    double time;
    double specular;
    double distance;
    Color color;
    Color light_color;
    bool is_source;
    Vector normal;
    Vector intersection_point;
    Vector origin;
    Vector direction;
    uint32_t object_id;
    uint8_t osx;
    uint8_t osy;
    uint8_t reflection_counter;

    Ray(class Scene *scene, class Vector &origin, class Vector &direction, uint8_t osx, uint8_t osy, double time);
    void evaluate_distance(uint32_t object_id, Vector &point, Color &color, Vector &normal, bool is_source, double specular);
    void evaluate_ilumination(Vector point, uint32_t source_id);
    void end_evaluate_ilumination(void);
    void evaluate_specular(void);
};

#endif // _RAY_H_
