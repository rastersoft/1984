// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#include <cstdio>

#include "object3d.hh"
#include <math.h>

Object3d::Object3d(class Texture *texture, bool is_light_source) {
    this->complete_init(is_light_source);
    this->texture = texture;
}

Object3d::Object3d(Color color, bool is_light_source) {
    this->complete_init(is_light_source);
    this->color = color;
    this->texture = NULL;
}

void Object3d::complete_init(bool is_light_source) {
    this->next = NULL;
    this->visible = true;
    this->specular = 0.0;
    this->is_source = is_light_source;
    this->is_camera = false;
    this->current_time = -1;
    this->camera = NULL;
    this->applied_camera = false;
    this->reset_matrix();
}



void Object3d::set_camera(Camera *camera) {
    if (this->camera == NULL) {
        this->camera = camera;
    }
}

void Object3d::reset_matrix(void) {
    this->current_matrix.reset();
    this->current_inverse.reset();
    this->origin.set(0,0,0);
    this->applied_camera = false;
}


void Object3d::set_current_time(double time) {
    /** Specifies the new time instant, to allow to evaluate motion blur
        @param time The time value, in [0, 1] range, being 0 the instant when the shutter opens, and 1 when it closes
    */

    // by default, objects are fully static. Moving objects must inherite from the class and overwrite this method,
    // setting the current matrix with the right value for the time instant

    // This method is called every time a ray is evaluated (to avoid having to relocate objects that aren't being
    // evaluated), so it is paramount to store the instant in this->current_time, and don't recalculate the matrixes
    // unless the value is different.

    this->current_time = time;
}


void Object3d::apply_camera(void) {
    if ((this->camera == NULL) || this->applied_camera) {
        return;
    }

    this->current_matrix.rdot(this->camera->get_current_matrix());
    this->current_inverse.ldot(this->camera->get_current_inverse());
    this->origin.values[0] = 0;
    this->origin.values[1] = 0;
    this->origin.values[2] = 0;
    this->origin *= this->current_matrix;
    this->applied_camera = true;
}

void Object3d::get_color_at(double u, double v, Color &color) {
    /** Returns the pixel value for the coordinates U,V, being both float values in range [0, 1]
        It uses the pixels from the texture if available, of the color if there is no texture.

        @param coordinates a 2D tuple with the U and V coordinates in the texture
        @return a 4-element tupla with the RGBA value
    */

    if (this->texture == NULL) {
        color = this->color;
        return;
    }

    this->texture->get_color_at(u,v,color);
}

void Object3d::transform_normal_coordinates(Vector &normal) {
    /** Transforms the normal into the scene coordinates
        @param normal The normal in object coordinates (will be modified)
    */

    // Add the origin to transform it into a point in 3D space
    // (but in object coordinates it is (0,0,0), so that's why there isn't normal + origin)
    // transform it to scene coordinates, and substract the transformed origin to convert
    // it again in a direction vector,

    normal *= this->current_matrix;
    normal -= this->origin;
    // it must be an unitary vector
    normal.unitary();
}


void Object3d::ray_to_object_coords(class Ray &ray, Vector &origin, Vector &direction) {
        /** Transforms the origin and direction vectors of a ray (which are in scene coordinates)
            into object coordinates.

            @param ray A ray
            @param origin A vector where the transformed origin will be stored
            @param direction A vector where the transformed direction will be stored
        */

        origin = ray.origin;
        origin *= this->current_inverse;
        direction = ray.direction + ray.origin;
        direction *= this->current_inverse;
        direction -= origin;
        direction.unitary();
}


void Object3d::get_light(class Ray &ray, uint8_t oversamplingX, uint8_t oversamplingY, Vector &retval) {

    /** This call must be overriden by each new object.

        Returns a point from the surface, to calculate ilumination
        @param ray The ray which is being evaluated for ilumination
        @param oversamplingX The oversampling value in the X axis
        @param oversamplingY The oversampling value in the Y axis
        @return an np.array with a point from the surface of the object
    */

    // before everything, update the position acording to the time instant of the ray being evaluated
    this->set_current_time(ray.time);

    // apply the camera transformation if needed
    this->apply_camera();

}

void Object3d::evaluate(class Ray &ray) {
    /** This call must be overriden by each new object. It must evaluate
        the intersection of the object with the ray to set the color
        inside it */

    // before everything, update the position acording to the time instant of the ray being evaluated
    this->set_current_time(ray.time);

    // apply the camera transformation if needed
    this->apply_camera();
}

void Object3d::print() {
    this->current_matrix.print();
    this->current_inverse.print();
}
