// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

/*
    This object is an sphere, which currently can only have a single color. You must
    create a new object for each object that you want to add to an scene.

    All spheres, by default, are located at the origin and have a radius of 1.
    It is possible to change this by using the ROTATE, TRANSLATE and SCALE
    methods.
*/

#include <cstdio>

#include "sphere.hh"
#include "defines.hh"

Sphere::Sphere(class Texture* texture, bool is_source):Object3d(texture, is_source) {
        /*Initializates the plane
            @param texture The texture or color for the surface
            @param is_source A boolean specifying if this object is a light source
        */
}

Sphere::Sphere(class Color color, bool is_source):Object3d(color, is_source) {}

void Sphere::point_to_texture(Vector &point, Vector &uv) {

        /* Receives an intersection point and returns the angular coordinates, for the texture
            @param point The intersection point. It must be in the surface of the sphere
            @return a vector where the X and Y coordinates have with the texture coordinates in the range [0,1]
        */

        double x = point.values[0];
        double y = point.values[1];
        double z = point.values[2];
        // just in case. Values should never be outside [-1,1], but rounding errors can produce them
        y = MAX(-1, y);
        y = MIN(y, 1);
        double v = asin(y); // the arcsin of Y is the angle
        double tmp = cos(v); // the radius of the circle in that point
        v /= PI;
        v += 0.5;
        uv.values[1] = v;

        if (tmp == 0) {
            uv.values[0] = 0.0;
            return;
        }
        // normalize X and Z
        x = MAX(-1, x);
        x = MIN(x, 1);
        z = MAX(-1, z);
        z = MIN(z, 1);
        x /= tmp;
        z /= tmp;
        double u = acos(x);
        if (z <= 0) {
            u = (2 * PI) - u;
        }
        u /= (2 * PI);
        uv.values[0] = u;
        return;
}

void Sphere::get_light(class Ray &ray, uint8_t oversamplingX, uint8_t oversamplingY, Vector &point) {
    /* Returns a point from the surface, to calculate ilumination
        @param ray The ray which is being evaluated for ilumination
        @param oversamplingX The oversampling value in the X axis
        @param oversamplingY The oversampling value in the Y axis
        @return an np.array with a point from the surface of the sphere
    */

    Object3d::get_light(ray,oversamplingX, oversamplingY, point);

    double a = (RANDOM * PI * double(ray.osx)) / double(oversamplingX);
    double b = (RANDOM * PI * 2.0 * double(ray.osy)) / double(oversamplingY);

    double y = sin(a);
    double radius = cos(a);
    double z = radius * sin(b);
    double x = radius * cos(b);

    point.values[0] = x;
    point.values[1] = y;
    point.values[2] = z;
    point *= this->current_matrix;
}


void Sphere::evaluate(class Ray &ray) {
    /* Evaluates a ray, checking if the intersection with this sphere is the nearest one and
        setting the current color

        @param ray A Ray object to evaluate its intersection
    */

    Vector o;
    Vector l;

    Object3d::evaluate(ray);

    // First, take the ray values (which are in scene coordinates) and transform them into
    // object coordinates.
    this->ray_to_object_coords(ray, o, l);

    // Now calculate the intersection between the transformed ray and an sphere of radius 1 located
    // at (0,0,0)
    double tmp = pow(Vector::dot(l, o), 2) - (pow(Vector::norm(o),2) - 1.0);
    if (tmp < 0) {
        // No intersection
        return;
    }
    tmp = sqrt(tmp);
    double tmp2 = -(Vector::dot(l, o));
    if ((tmp2 - tmp) >= 0) {
        // Only if tmp2-tmp is positive, the ray goes "forward"; reject if it goes "backwards" because that
        // means that the intersection is behind the origin point
        Vector intersection = o + l * (tmp2 - tmp);
        Vector normal(intersection); // since the sphere has radius 1 (in object coordinates) the intersection point is the same than the normal direction
        this->transform_normal_coordinates(normal);
        Vector point(intersection);
        point *= this->current_matrix; // get the true intersection coordinates in scene space
        Vector uv;
        this->point_to_texture(intersection, uv);
        Color color;
        this->get_color_at(uv.values[0], uv.values[1], color);
        ray.evaluate_distance(this->identifier, point, color, normal, this->is_source, this->specular);
    }

    if ((tmp != 0) && ((tmp2 + tmp) >= 0)) {
        // if tmp is 0, the ray is tangent (so there is a single intersection point);
        // if it is greater than 0, it intersects in two points
        Vector intersection = o + l * (tmp2 + tmp);
        Vector normal(intersection);
        this->transform_normal_coordinates(normal);
        Vector point(intersection);
        point *= this->current_matrix; // get the true intersection coordinates in scene space
        Vector uv;
        this->point_to_texture(intersection, uv);
        Color color;
        this->get_color_at(uv.values[0], uv.values[1], color);
        ray.evaluate_distance(this->identifier, point, color, normal, this->is_source, this->specular);
    }
}
