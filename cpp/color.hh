// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef _COLOR_H_
#define _COLOR_H_

#include <stdint.h>

class Color {

private:
    float lin_to_srgb(float v, float gamma);
    float srgb_to_lin(float v, float gamma);

public:

    float r;
    float g;
    float b;
    float a;

    Color();
    Color(float r, float g, float b, float a);
    void operator=(Color const &);
    void set(float r, float g, float b, float a);
    void set_int(int r, int g, int b, int a);
    void apply_gamma(float);
    void operator*=(float);
    Color operator*(float);
    void operator*=(Color const &);
    void operator/=(float);
    void operator+=(Color const &);
    void operator+=(float);
    void print(bool);
};

#endif //_COLOR_H_
