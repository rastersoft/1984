// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef _RENDER_H_
#define _RENDER_H_

#include <time.h>
#include "texture.hh"

class Render {

protected:
    double distance;
    time_t start_time;
    Texture *data;

    uint32_t width;
    uint32_t height;
    uint8_t oversamplingX;
    uint8_t oversamplingY;
    uint8_t total_oversampling;

    void print_current_line(uint32_t);
    void render_line(double y);

public:
    class Scene *scene;
    double fov;

    Render(class Scene *, uint32_t width, double aspect = 16.0/9.0, double angle = 45);
    void set_oversampling(uint8_t, uint8_t);
    virtual void render(void);
    void show();
    void save(const char *);
};

#endif // _RENDER_H_
