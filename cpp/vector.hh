// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef _VECTOR_H_
#define _VECTOR_H_

#include "matrix.hh"

class Vector {

public:
    double values[4];

    Vector();
    Vector(double, double, double);
    Vector(Vector const &);
    void operator=(Vector const &);
    void set(double x, double y, double z);
    void operator*=(Matrix const &);
    void operator+=(Vector const &);
    const Vector operator+(Vector const &);
    void operator-=(Vector const &);
    Vector operator-(Vector const &);
    void operator*=(double);
    Vector operator*(double);
    void unitary();
    static double norm(Vector const &);
    static double dot(Vector const &, Vector const &);
    void print(bool newline);
};

#endif // _VECTOR_H_
