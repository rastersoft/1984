// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef _SUBOBJECT_H_
#define _SUBOBJECT_H_

#include <stdint.h>
#include <math.h>
#include <cstddef>

#include "matrix.hh"
#include "vector.hh"

class SubObject3d {
protected:
    Matrix current_matrix;
    Matrix current_inverse;

public:
    bool is_camera;

    void rotate_x(double angle);
    void rotate_y(double angle);
    void rotate_z(double angle);
    void translate(double x, double y, double z);
    void scale(double x, double y, double z);
};

#endif //_SUBOBJECT_H_
