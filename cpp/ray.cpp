// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#include <cstdio>

#include "ray.hh"
#include <math.h>

Ray::Ray(class Scene *scene, Vector &origin, Vector &direction, uint8_t osx, uint8_t osy, double time) {
    /** Defines a ray, used to evaluate visibility with objects.
        @param scene The current scene
        @param origin The coordinates of the point where the ray originated
        @param direction A directional vector specifying the direction of the ray
        @param osx Oversampling coordinate in the X axis
        @param oxy Oversampling coordinate in the Y axis
        @param time The time instant that corresponds to this ray

        The oversampling coordinates allow to ensure a good ray distribution
        when having to evaluate lights.
    */

    this->scene = scene;
    this->light_sources = 0;

    this->time = time;
    this->color.set(0,0,0,1);
    this->light_color.set(0,0,0,0);
    this->is_source = false;
    this->specular = 0;
    this->distance = INFINITY;
    this->object_id = 0;
    this->origin = origin;
    this->direction = direction;
    this->direction.unitary();
    this->osx = osx;
    this->osy = osy;
    this->reflection_counter = 0;
}


void Ray::evaluate_distance(uint32_t object_id, Vector &point, Color &color, Vector &normal, bool is_source, double specular) {
    /** Receives the intersection point, calculates the distance to the origin, and returns True if it is
        nearer than the last calculated distance

        @param object_id the identifier of the object being evaluated
        @param point A 3D np.array with the intersection point to evaluate
        @param color The new color to assign to the ray if this point is nearer than the previous
        @param is_source Whether the object is a light source or not
        @param specular A value between 0 and 1 that specifies how much specular has the object (0 no specular; 1 mirror) */

    Vector tmp(point);
    tmp -= this->origin;
    double distance = Vector::norm(tmp);
    if (distance == 0) {
        // the origin point must not be evaluated to allow to trace rays from one point of an object
        // to another point of the same object, like for refraction
        return;
    }
    if ((this->distance == INFINITY) || (this->distance > distance)) {
        this->object_id = object_id;
        this->distance = distance;
        this->intersection_point = point;
        this->color = color;
        this->normal = normal;
        this->is_source = is_source;
        this->specular = specular;
    }
}



void Ray::evaluate_ilumination(Vector point, uint32_t source_id) {

    // there is no intersection with any of the objects in the scene
    if (this->object_id == 0) {
        return;
    }

    if (this->is_source) {
        // a light source looks always iluminated, with the maximum intensity
        return;
    }

    // create a ray from the intersection point pointing to the light source's point
    // and evaluate if there is another object in-between

    Vector direction = point - this->intersection_point;
    direction.unitary();
    // separate slightly the intersection point from the surface to avoid detecting it
    Vector origin = this->intersection_point + direction * 0.001;
    Ray ray(this->scene, origin, direction, this->osx, this->osy, this->time);

    this->scene->evaluate_ray(ray);

    // if the nearest object is the same than source_id, then there are no objects
    // between this point and the light source, so it is iluminated

    if (ray.object_id == source_id) {
        // we calculate the cross product between the surface normal and the direction,
        // because it is proportional to the light intensity (Lambert's cosine theorem)

        double intensity = Vector::dot(this->normal, direction);
        if (intensity < this->scene->ambient) {
            intensity = this->scene->ambient;
        }
        ray.color *= intensity;
        this->light_color += ray.color;
    } else {
        // The object is in shadow
        this->light_color += this->scene->ambient;
    }
    this->light_sources++;
}

void Ray::end_evaluate_ilumination(void) {
    if (this->light_sources != 0) {
        this->light_color /= this->light_sources;
        this->color *= this->light_color;
    }
}


void Ray::evaluate_specular(void) {
    // if the object is absolutely not reflectant, don't waste resources evaluating it
    if (this->specular <= 0) {
        return;
    }

    if (this->reflection_counter == this->scene->max_reflections) {
        if (this->specular >= 1) {
            this->color.set(0,0,0,1);
        }
        return;
    }

    // Calculate the reflected direction
    Vector rdirection(this->normal);
    rdirection *= (-2.0 * Vector::dot(this->direction, this->normal));
    rdirection += this->direction;

    // Create a new ray
    Vector origin = this->intersection_point + rdirection * 0.0001;
    Ray ray(this->scene, origin, rdirection, this->osx, this->osy, this->time);
    ray.reflection_counter = this->reflection_counter + 1;
    // evaluate the reflected ray
    this->scene->evaluate(ray);
    this->color *= (1.0 - this->specular);
    this->color += (ray.color * this->specular);
}
