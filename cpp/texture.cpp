// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#include "texture.hh"
#include <stdint.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <CImg.h>

#include "defines.hh"

using namespace cimg_library;

Texture::Texture() {}

Texture::Texture(const char *filename) {
    printf("Loading %s\n", filename);
    CImg<unsigned char> *image = new CImg<unsigned char>(filename);
    this->data = image->data();
    this->width = image->width();
    this->height = image->height();
    this->psize = this->width * this->height;
    this->depth = image->spectrum();
    this->image = image;
    this->sharedmem = false;
}

Texture::Texture(uint32_t width, uint32_t height, bool sharedmem) {
    this->width = width;
    this->height = height;
    this->psize = width * height;
    this->depth = 3;
    CImg<unsigned char> *image = new CImg<unsigned char>(width, height, 1, 3);
    this->image = image;
    this->sharedmem = sharedmem;
    if (sharedmem) {
        this->data = (uint8_t *)mmap(NULL, width * height * 3, PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS, 0, 0);
    } else {
        this->data = image->data();
    }
}

void Texture::store(const char *filename) {
    CImg<unsigned char> *image = (CImg<unsigned char> *) this->image;
    if (this->sharedmem) {
        memcpy(image->data(), this->data, this->psize * 3);
    }
    image->save_png(filename);
}

void Texture::show() {
    CImg<unsigned char> *image = (CImg<unsigned char> *) this->image;
    if (this->sharedmem) {
        memcpy(image->data(), this->data, this->psize * 3);
    }
    CImgDisplay main_disp(*image);
    while(!main_disp.is_closed()) {
        main_disp.wait();
    }
    main_disp.close();
}

void Texture::set_color_at(uint32_t x, uint32_t y, Color &color) {
    uint32_t index = x + y * this->width;
    uint8_t *p = this->data + index;
    *p = uint8_t(color.r * 255);
    p += this->psize;
    *p = uint8_t(color.g * 255);
    p += this->psize;
    *p = uint8_t(color.b * 255);
}

void Texture::get_color_at(double u, double v, Color &color) {
    uint32_t y = MIN(uint32_t(v * this->height), this->height - 1);
    uint32_t x = MIN(uint32_t(u * this->width), this->width - 1);
    uint32_t index = (x + this->width * y);
    uint8_t *pixel = this->data + index;
    uint8_t r = *pixel;
    pixel += this->psize;
    uint8_t g = *pixel;
    pixel += this->psize;
    uint8_t b = *pixel;
    color.set_int(r, g, b, 255);
}
