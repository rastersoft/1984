// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef _SPHERE_H_
#define _SPHERE_H_

#include <stdint.h>
#include <cstddef>
#include "texture.hh"
#include "object3d.hh"
#include "color.hh"

class Sphere: public Object3d {

private:
    Vector normal;
    void point_to_texture(Vector &, Vector &);

public:
    Sphere(class Texture*, bool = false);
    Sphere(class Color , bool = false);
    virtual void get_light(class Ray &, uint8_t oversamplingX, uint8_t oversamplingY, Vector &);
    virtual void evaluate(class Ray &);
};

#endif // _SPHERE_H_
