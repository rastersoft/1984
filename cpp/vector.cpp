// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#include "vector.hh"
#include "matrix.hh"
#include <math.h>
#include <cstdio>

Vector::Vector() {
    this->values[0] = 0;
    this->values[1] = 0;
    this->values[2] = 0;
    this->values[3] = 1;
}

Vector::Vector(Vector const &vector) {
    this->values[0] = vector.values[0];
    this->values[1] = vector.values[1];
    this->values[2] = vector.values[2];
    this->values[3] = 1;
}

Vector::Vector(double x, double y, double z) {
    this->values[0] = x;
    this->values[1] = y;
    this->values[2] = z;
    this->values[3] = 1;
}

void Vector::operator=(Vector const &o) {
    this->values[0] = o.values[0];
    this->values[1] = o.values[1];
    this->values[2] = o.values[2];
}

void Vector::set(double x, double y, double z) {
    this->values[0] = x;
    this->values[1] = y;
    this->values[2] = z;
}

void Vector::operator*=(Matrix const &matrix) {
    // only need to operate with the first three elements
    double output[3];
    double acumulator;
    const double *p2 = matrix.values;
    double *p1, *p3 = output;
    int x, y;
    // The fourth element in a quaternion is always 1, so don't calculate it
    for(x=0; x<3; x++) {
        acumulator = 0;
        p1 = this->values;
        for(y=0;y<4;y++) {
            acumulator += (*p1) * (*p2);
            p1++;
            p2 += 4;
        }
        p2 -= 15;
        *p3 = acumulator;
        p3++;
    }
    for(x=0, p1=output, p3=this->values; x < 3; x++) {
        *(p3++) = *(p1++);
    }
}

void Vector::operator+=(Vector const &vector) {
    this->values[0] += vector.values[0];
    this->values[1] += vector.values[1];
    this->values[2] += vector.values[2];
}

const Vector Vector::operator+(Vector const &vector) {
    Vector v;
    v.values[0] = this->values[0] + vector.values[0];
    v.values[1] = this->values[1] + vector.values[1];
    v.values[2] = this->values[2] + vector.values[2];
    return v;
}

void Vector::operator-=(Vector const &vector) {
    this->values[0] -= vector.values[0];
    this->values[1] -= vector.values[1];
    this->values[2] -= vector.values[2];
}

Vector Vector::operator-(Vector const &vector) {
    Vector v;
    v.values[0] = this->values[0] - vector.values[0];
    v.values[1] = this->values[1] - vector.values[1];
    v.values[2] = this->values[2] - vector.values[2];
    return v;
}

void Vector::operator*=(double value) {
    this->values[0] *= value;
    this->values[1] *= value;
    this->values[2] *= value;
}

Vector Vector::operator*(double value) {
    Vector v;
    v.values[0] = this->values[0] * value;
    v.values[1] = this->values[1] * value;
    v.values[2] = this->values[2] * value;
    return v;
}

double Vector::dot(Vector const &vector1, Vector const &vector2) {
    double acumulator;
    acumulator  = vector1.values[0] * vector2.values[0];
    acumulator += vector1.values[1] * vector2.values[1];
    acumulator += vector1.values[2] * vector2.values[2];
    return acumulator;
}

double Vector::norm(Vector const &vector) {
    double acumulator;
    acumulator  = vector.values[0] * vector.values[0];
    acumulator += vector.values[1] * vector.values[1];
    acumulator += vector.values[2] * vector.values[2];
    return sqrt(acumulator);
}

void Vector::unitary() {
    double n = Vector::norm(*this);
    this->values[0] /= n;
    this->values[1] /= n;
    this->values[2] /= n;
}

void Vector::print(bool newline) {
    printf("(%f,%f,%f,%f)%s", this->values[0], this->values[1], this->values[2], this->values[3], newline ? "\n" : " ");
}
