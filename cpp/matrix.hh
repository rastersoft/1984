// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef _MATRIX_H_
#define _MATRIX_H_

class Matrix {

private:
    void dot(Matrix *, Matrix *);

public:
    double values[16];

    Matrix();
    void reset();
    void ldot(Matrix *); // implements matrix * this
    void rdot(Matrix *); // implements this * matrix
    void print();
};

#endif // _MATRIX_H_
