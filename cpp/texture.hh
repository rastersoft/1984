// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include <stdint.h>
#include <cstddef>
#include "color.hh"

class Texture {

private:
    uint8_t *data;
    uint32_t width;
    uint32_t height;
    uint32_t psize;
    uint8_t depth;
    void *image; // put it as a void pointer to avoid adding here CImg.h (this trick makes compilation much faster)
    bool sharedmem;

public:
    Texture();
    Texture(uint32_t, uint32_t, bool sharedmem = false);
    Texture(const char *);
    void get_color_at(double, double, Color &);
    void set_color_at(uint32_t, uint32_t, Color &);
    void store(const char *);
    void show();
};

#endif // _TEXTURE_H_
