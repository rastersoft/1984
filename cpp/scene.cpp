// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#include "scene.hh"
#include <cstdio>

Scene::Scene() {
    this->elements = NULL;
    this->light_sources = NULL;
    this->id_counter = 1;
    this->ambient = 0.15;
    this->oversamplingX = 1;
    this->oversamplingY = 1;
    this->camera = NULL;
    this->max_reflections = 3;
}


void Scene::set_oversampling(uint8_t overX, uint8_t overY) {
    this->oversamplingX = overX;
    this->oversamplingY = overY;
}


void Scene::append(Object3d *obj) {
    if (obj->is_camera) {
        printf("Trying to add a camera as a 3D object");
        throw -1;
    }
    obj->identifier = this->id_counter;
    this->id_counter++;
    if (this->camera!=NULL) {
        obj->set_camera(this->camera);
    }
    if (obj->is_source) {
        obj->next = this->light_sources;
        this->light_sources = obj;
    } else {
        obj->next = this->elements;
        this->elements = obj;
    }
}


void Scene::set_camera(Camera *camera) {
    if (!camera->is_camera) {
        printf("Trying to add a 3D object as camera");
        throw -2;
    }

    if (this->camera == NULL) {
        this->camera = camera;
        for(Object3d*element = this->elements; element != NULL; element = element->next) {
            element->set_camera(camera);
        }
        for(Object3d*source = this->light_sources; source != NULL; source = source->next) {
            source->set_camera(camera);
        }
    } else {
        printf("There is already a camera in the scene. Aborting");
        throw -3;
    }
}


void Scene::evaluate(class Ray &ray) {
    /*Evaluates a ray and returns a Ray object with the result
        @param ray A Ray object with the ray to be evaluated
    */

    // test intersection with all the objects in the scene (except invisible lights)
    this->evaluate_ray(ray, false);

    // now evaluate the ilumination in the nearest object

    if (ray.specular < 1.0) {
        // evaluate the ilumination only if the object is not 100% reflectant
        for(Object3d *source = this->light_sources; source != NULL; source = source->next) {
            // Ask the light source for a random point in its surface and the corresponding color
            Vector point;
            source->get_light(ray, this->oversamplingX, this->oversamplingY, point);
            // And evaluate the color in that point
            ray.evaluate_ilumination(point, source->identifier);
        }
        ray.end_evaluate_ilumination();
    }

    // now evaluate the reflected ray
    ray.evaluate_specular();
}

void Scene::evaluate_ray(class Ray &ray, bool check_invisible) {
    /*Evaluates a ray against all the objects
        @param ray The ray with the origin point and the direction
    */
    // test intersection with all the objects in the scene
    for(Object3d *element = this->elements; element != NULL; element = element->next) {
        element->evaluate(ray);
    }
    for(Object3d *source = this->light_sources; source != NULL; source = source->next) {
        if ((source->visible) || check_invisible) {
            source->evaluate(ray);
        }
    }
}

void Scene::save(const char *filename) {}
