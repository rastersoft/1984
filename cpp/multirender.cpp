// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#include <cstdio>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/mman.h>

#include "multirender.hh"

Multirender::Multirender(class Scene *scene, uint32_t width, double aspect, double angle) : Render::Render(scene, width, aspect, angle) {}

void Multirender::set_processes() {
    /** Autodetects the number of cores in the system */

    FILE *cpuinfo = fopen("/proc/cpuinfo", "r");
    if (!cpuinfo) {
        printf("Can't detect the number of processors. Aborting.\n");
        exit(-1);
    }
    char buffer[10];
    bool wait_for_cr = false;
    this->processors = 0;
    while(!feof(cpuinfo)) {
        if (wait_for_cr) {
            fread(buffer, 1, 1, cpuinfo);
            if (buffer[0] == '\n') {
                wait_for_cr = false;
                continue;
            }
        }
        for(int i=0; i<9; i++) {
            fread(buffer+i, 1, 1, cpuinfo);
            buffer[i+1] = 0;
            if (buffer[i] == '\n') {
                break;
            }
        }
        if (0 == strcmp(buffer, "processor")) {
            this->processors++;
        }
        buffer[8] = 0;
        wait_for_cr = true;
    }
    fclose(cpuinfo);
    if (this->processors == 0) {
        this->processors = 1;
    }
}


void Multirender::render(int processes) {
    /*Renders the current scene in parallel
        @param processes The number of parallel processes to use; 0 = autodetect
    */

    if (processes == 0) {
        this->set_processes();
    } else {
        this->processors = processes;
    }

    // this->data will contain the pixels
    this->data = new Texture(this->width, this->height, true);

    this->start_time = time(NULL);

    this->y = 0;
    int *pids = new int[this->processors];

    // The data that will be shared between processes: the last rendered line, and a semaphore to access it.
    this->sdata = (struct shared_data *)mmap(NULL, sizeof(struct shared_data), PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS, 0, 0);
    this->sdata->y = 0;
    sem_init(&this->sdata->sem, 1, 1);

    for(int p=0; p<this->processors; p++) {
        // Something odd happens with pthreads (it is even slower than single thread),
        // so we use fork()
        pids[p] = fork();
        if (pids[p] == 0) {
            this->child_render();
            exit(0);
        }
    }

    for(int p=0; p<this->processors; p++) {
        int status;
        waitpid(pids[p],&status,0);
    }
    printf("\n");
}

void Multirender::child_render() {

    // If there are X cores, each child renders
    // one of each X lines. This saves a lot of
    // work in IPC

    uint32_t y;

    while(1) {
        sem_wait(&this->sdata->sem);
        y = this->sdata->y;
        this->sdata->y++;
        sem_post(&this->sdata->sem);
        if (y >= this->height) {
            break;
        }
        this->print_current_line(y);
        this->render_line(y);
        y += this->processors;
    }
}
