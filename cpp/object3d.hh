// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef _OBJECT3D_H_
#define _OBJECT3D_H_

#include <stdint.h>
#include <cstddef>

#include "color.hh"
#include "matrix.hh"
#include "vector.hh"
#include "camera.hh"
#include "texture.hh"
#include "subobject3d.hh"
#include "ray.hh"

class Object3d: public SubObject3d {
    /** Base class for every object in the scene.
        It contains the current transform matrix and its inverse. This allows
        to easily convert object points into space points and vice-versa.
        It also offers several functions to modify both matrixes, like rotate,
        translate and scale. */

private:
    void complete_init(bool is_light_source);
    void apply_camera();

protected:
    class Color color;
    class Camera *camera;
    class Texture *texture;
    class Vector origin;
    float current_time;
    bool applied_camera;

public:
    Object3d *next;
    float specular;
    uint32_t identifier;
    bool visible;
    bool is_source;

    Object3d(class Texture *texture, bool is_light_source = false);
    Object3d(class Color color, bool is_light_source = false);

    void set_camera(class Camera *);
    void reset_matrix(void);
    void transform_normal_coordinates(class Vector &);
    void ray_to_object_coords(class Ray &, Vector &, Vector &);
    virtual void set_current_time(double);
    virtual void get_color_at(double u, double v, Color &);
    virtual void get_light(class Ray &, uint8_t, uint8_t, Vector &);
    virtual void evaluate(class Ray &);

    void print();

};

#endif //_OBJECT3D_H_
