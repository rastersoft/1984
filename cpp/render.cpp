// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#include <cstdio>
#include <time.h>
#include <cstring>

#include "defines.hh"
#include "ray.hh"
#include "render.hh"

Render::Render(class Scene *scene, uint32_t width, double aspect, double angle) {
    /*Creates a renderer for the specified scene.
        @param scene The scene to render
        @param width The width of the output image in pixels
        @param aspect The aspect ratio for the output image
        @param angle The Field of Vision in degrees
    */

    this->scene = scene;
    this->fov = angle;
    this->width = width;
    this->height = int(width / aspect);
    if (aspect < 1) {
        this->distance = this->height / (2 * tan(angle * PI / 360));
    } else {
        this->distance = this->width / (2 * tan(angle * PI / 360));
    }
    // By default, no oversampling
    this->oversamplingX = 1;
    this->oversamplingY = 1;
    this->total_oversampling = 1;
    this->data = NULL;
}


void Render::set_oversampling(uint8_t x, uint8_t y) {
    /*Allows to set the oversampling for each pixel, in each of the two dimensions
        (it is common to set the same value in both parameters, but this gives more flexibility)

        @param x Oversampling in the X axis
        @param y Oversampling in the Y axis
    */
    this->oversamplingX = x;
    this->oversamplingY = y;
    this->total_oversampling = x * y;
    this->scene->set_oversampling(x, y);
}


void Render::render(void) {
    /*Renders the current scene with a single core*/

    // this->data will contain the pixels
    this->data = new Texture(this->width, this->height);

    this->start_time = time(NULL);

    // Do the tracing from -width/2 to width/2, and -height/2 to height/2;
    // this way the (0,0,0) coordinate is in the center of the screen
    for (uint32_t y = 0; y < this->height; y++) {
        this->print_current_line(y);
        this->render_line(y);
    }
    printf("\n");
}


void Render::print_current_line(uint32_t y) {

    time_t now = time(NULL);
    if ((now != this->start_time) && (y > 15)) {
        int remaining = int((this->height - y) * (now - this->start_time) / y);
        printf("%d of %d. Running for %d seconds. ERT: %d seconds.            \r", y , this->height, now - this->start_time, remaining);
    } else {
        printf("%d of %d. Running for %d seconds.\r",y , this->height, now - this->start_time);
    }
    fflush(stdout);
}

void Render::render_line(double y) {

    /*Renders a single scanline
        @param y The line number to render
        @return a list with color tuples, corresponding to the rendered line
    */

    double w2 = this->width / 2;
    double h2 = this->height / 2;

    for(double x = 0; x < this->width; x++) {
        // Now evaluate each ray that starts from the eye and passes through each pixel
        // By default, the eye is located at (0,0,0), and the distance between
        // the eye and the proyection plane is twice the height, to avoid
        // deformations in the objects located in the borders

        Color current_color;

        for (uint8_t overx = 0; overx < this->oversamplingX; overx++) {
            for (uint8_t overy = 0; overy < this->oversamplingY; overy++) {
                // Each pixel is oversampled with oversamplingX * oversamplingY rays
                // The pixel is divided in oversamplingX x oversamplingY subpixels and
                // a ray is traced for each square. The precise point in the subpixel
                // is choosen randomly, to make it a pseudo-poison disc distribution
                double ox = ((double)overx) + (RANDOM * 0.75);
                double oy = ((double)overy) + (RANDOM * 0.75);

                ox /= double(this->oversamplingX);
                oy /= double(this->oversamplingY);

                // Calculate the time interval for this ray
                // Divide the time in oversamplingX * oversamplingY intervals and
                // distribute the rays into them, one for each interval.
                // In each interval, the precise instant is random, like in the spatial
                // oversampling, to have again a pseudo-poison disc distribution
                double time = ((RANDOM * 0.75) + double(overx + overy * this->oversamplingX)) / double(this->total_oversampling);

                Vector origin; // (0,0,0)
                Vector direction(x - w2 + ox, y - h2 + oy, this->distance);
                Ray ray(this->scene, origin, direction, overx, overy, time);
                this->scene->evaluate(ray);
                current_color += ray.color;
            }
        }

        // Calculate the average of all the rays sent for this pixel
        current_color /= this->total_oversampling;
        this->data->set_color_at(x, y, current_color);
    }
}

void Render::show() {
     this->data->show();
}


void Render::save(const char* filename) {
    this->data->store(filename);
}
