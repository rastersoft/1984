// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef _MULTIRENDER_H_
#define _MULTIRENDER_H_

#include <semaphore.h>

#include "render.hh"

struct shared_data {
    // This is used to store the data shared between processes
    sem_t sem;
    uint32_t y;
};

class Multirender: public Render {

private:
    uint8_t processors;
    uint32_t y;
    struct shared_data *sdata;

    void set_processes(void);

public:
    Multirender(class Scene *, uint32_t width, double aspect = 16.0/9.0, double angle = 45);
    void render(int = 0);
    void child_render();
};

#endif // _MULTIRENDER_H_
