// Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
//
// This file is part of '1984 RayTracer'
//
// '1984 RayTracer' is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// '1984 RayTracer' is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#include <cstdio>
#include <math.h>

#include "color.hh"

Color::Color() {
    this->r = 0.0;
    this->g = 0.0;
    this->b = 0.0;
    this->a = 0.0;
}

Color::Color(float r, float g, float b, float a) {
    this->r = r;
    this->g = g;
    this->b = b;
    this->a = a;
}

void Color::operator=(Color const &color) {
    this->r = color.r;
    this->g = color.g;
    this->b = color.b;
    this->a = color.a;
}

void Color::set(float r, float g, float b, float a) {
    this->r = r;
    this->g = g;
    this->b = b;
    this->a = a;
}

void Color::set_int(int r, int g, int b, int a) {
    this->r = float(r) / 255.0;
    this->g = float(g) / 255.0;
    this->b = float(b) / 255.0;
    this->a = float(a) / 255.0;
}

void Color::apply_gamma(float gamma) {
    this->r = this->lin_to_srgb(this->r, gamma);
    this->g = this->lin_to_srgb(this->g, gamma);
    this->b = this->lin_to_srgb(this->b, gamma);
    this->a = this->lin_to_srgb(this->a, gamma);
}

float Color::lin_to_srgb(float v, float gamma) {
    if (v < 0.0031308) {
        return 12.92 * v;
    } else {
        return (1.055 * pow(v, 1 / gamma) - 0.55);
    }
}

float Color::srgb_to_lin(float v, float gamma) {
    if (v < 0.04045) {
        return v / 12.92;
    } else {
        return pow((v + 0.055) / 1.055, gamma);
    }
}

void Color::operator*=(float value) {
    this->r *= value;
    this->g *= value;
    this->b *= value;
}

Color Color::operator*(float value) {
    Color color;
    color.r = this->r * value;
    color.g = this->g * value;
    color.b = this->b * value;
    return color;
}

void Color::operator*=(Color const &color) {
    this->r *= color.r;
    this->g *= color.g;
    this->b *= color.b;
}

void Color::operator/=(float value) {
    this->r /= value;
    this->g /= value;
    this->b /= value;
}

void Color::operator+=(Color const &color) {
    this->r += color.r;
    this->g += color.g;
    this->b += color.b;
}

void Color::operator+=(float v) {
    this->r += v;
    this->g += v;
    this->b += v;
}

void Color::print(bool ret) {
    printf("Color(%d, %d, %d)%s",this->r, this->g, this->b, ret ? "\n" : " ");
}
