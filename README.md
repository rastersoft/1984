# 1984

A simple distributed raytracer, created as a little tribute to the authors of the original paper describing it: http://graphics.pixar.com/library/DistributedRayTracing/paper.pdf

## Background

35 years ago, a paper was published describing a then-new technique to improve Raytracing: the distributed raytracing. The basis was to distribute in space and time the same rays already used for antialiasing, allowing to achieve effects like soft shadows and penumbras, motion blur and depth of field without extra computer power.

That paper included a picture of five billiard balls to demonstrate the technique, and I wanted to reproduce it as a little tribute to the authors and their ingenuity. And this is the result:

![my tribute to 1984](1984.png)

How faithful is this picture? Well, the font used for the numbers in the balls isn't the same (only the 'one' in the first ball is similar). Of course, the textures for the billiard room are more simple (I can't compete with John Lasseter. Yes, he was the artist who painted them for the original picture). Also, it is rendered with 8x8 subsampling, when the original one was made with 4x4 (the result is less noisy, but needs four times the computing power). And certainly, the original is more realist. But hey...

## Python? C++?

In fact, both, but not in the sense you could expect. I created it originally in python, mainly because it was just an experiment, not something "serious", and since I wasn't even sure how to do it, I preferred to use an interpreted language, with automatic memory management, and a matrix and vector module like numpy. I added to it all what was needed to render a reproduction of the original 1984 image: recursive raytracing, antialias and random sampling with pseudo-moire disc, textures, soft shadows, motion blur and reflections; but didn't implement depth of field or blurry reflections. Maybe in a future...

Of course, after achieving the desired results, I found that the code was eeeexxxxtreeeeemmeeelyyyy sssslooooowwwww... To render it in 1920x1080 resolution, and a 8x8 subpixel grid for antialiasing, it needed 18 hours (yes... eighteen!!!!), but the code wasn't really complex, so I decided to try to port it to C++. The biggest problem was not having some matrix operations like inversion, but I found a way of doing it easy (just start with two unitary matrices, main and inverse, and apply to the second the opposite of the operation in the first one; so after rotating a radians in X by multiplying to the right the main matrix, also rotate -a radians in X by multiplying to the left the inverse matrix). It was quite straightforward after creating matrix and vector classes. Yes, probably I could have used any of the myriads of alternatives, but I suposed that using my own code would result in better performance because the matrices have a fixed size of 4x4, and using 4-D vectors but doing the norm and other operations only over the first three coordinates (and keeping the fourth one as a 1) could reduce the number of memory copies, and more.

And the result is that rendering the same 1920x1080 picture, with 8x8 subpixel grid, only requires 400 seconds in my i5 4440... less than seven minutes!!! Of course, using the four cores in the processor (the python version does it too).

## Using it

By default, the code will render and show the picture in a window. If you put a filename as a parameter, it will save the picture to that file.

    ./1984.py [outputfile.png]

or

    make
    ./1984 [outputfile.png]

To change parameters, you must edit the code. File *1984.py* for the python raytracer, and *main.cpp* for the c++ one. At the bottom of the files are the rendering parameters. Specifically, in *1984.py* there is:

        if True:
            r = Multirender(scene, 300)
        else:
            r = Render(scene, 320)
        #r.set_oversampling(8,8)
        r.render()

And in *main.cpp* there is:

        #if 1
            Multirender r(&scene, 1920);
        #else
            Render r(&scene, 600);
        #endif
            r.set_oversampling(8,8);
            r.render();

The *render* class renders the picture using a single core. It is the simplest code to understand. The *multirender* class renders them using all the cores. In both cases *fork()* is used; in python because, due to the GIL, there is no true multithreading; in C++, instead, I tried to use threads to simplify the code, but I had to give up because, although it should work, and it used 400% of the CPU, it needed **more** time than the single-core code; it was like the code was really using user-space threads, except for the fact that the kernel was aware of the four extra threads... Using *fork()*, instead, worked as expected and multiplied the rendering speed by nearly the same number as cores have my computer (four in my case).

The numeric parameter specifies the horizontal resolution. It is possible to add another parameter to specify the aspect ratio, but by default it is 16:9. A fourth parameter is possible to specify the projection angle (by default it is 45 degrees).

Finally, the *oversampling()* method allows the user to specify in how many subpixels will be divided each pixel. It is common to specify the same value in both, but I preferred to allow to choose the horizontal and vertical values, just in case.

And now, just compile it if you want to test the C++ version, or just launch it for the Python version, and enjoy.

## Can you implement...?

No.

I built this just as an exercise, not as something practical. It can be useful to learn how to (or how not to) implement a raytracer, but you won't be able to use it to beat Renderman or Arnold.

## The textures are...

I'm a programmer, not an artist. There are a lot of things that are beyond my capabilities.

## Under which license is distributed?

It is GPLv3 (or later).

## About the author

Sergio Costas Rodriguez
http://www.rastersoft.com
rastersoft@gmail.com
